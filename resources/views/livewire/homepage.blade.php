<div class="page-container-home">
    <div class="page-header-home">
        <div class="text-content">
            <h2 class="header-title">Sliders</h2>
            <div class="header-sub-title">
                <nav class="breadcrumb breadcrumb-dash">
                    <span class="breadcrumb-item active">Home</span>
                </nav>
            </div>
        </div>
    </div>
    <div class="container-admin container-add-home">
    <h5> Create Sliders</h5>
        <form wire:submit.prevent="submit">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <section class="form-group m-b-10">
                            <input placeholder="Heading" wire:model.lazy="fuck.0.get__heading" type="text" />
                            @error('fuck.*.get__heading') <label class="m-b-10" style="color: red">{{'Heading is Required'}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <input wire:model.lazy="fuck.0.get__subheading" placeholder="Sub heading" type="text" />
                            @error('fuck.*.get__subheading') <label class="m-b-00" style="color: red">{{'Sub-heading is Required'}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <input wire:model.lazy="fuck.0.get__image" type="file" />
                            {{-- @error('get__heading') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror --}}

                        </section>
                    </div>
                </div>
            </div>
            @foreach($inputs as $key => $value)
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <section class="form-group m-b-10">
                        <input wire:model.lazy="fuck.{{$value}}.get__heading" type="text" placeholder="Heading" />
                            @error('fuck.*.get__heading') <label class="m-b-10" style="color: red">{{'Heading is Required'}}</label> @enderror
                        </section>
                        <section class="form-group m-b-10">
                            <input wire:model.lazy="fuck.{{$value}}.get__subheading" placeholder="Sub heading" type="text" />
                            @error('fuck.*.get__subheading') <label class="m-b-10" style="color: red">{{'Sub-heading is Required'}}</label> @enderror
                        </section>
                        <section class="form-group m-b-10">
                            <input wire:model.lazy="fuck.{{$value}}.get__image" type="file"/>
                        </section>
                        <input class="btn btn-danger btn-sm" type="button" value="Remove" wire:click.prevent="remove({{$key}})">
                    </div>
                </div>
            </div>
            @endforeach
            <button type="button" wire:click="add({{$i}})" class="btn btn-hover font-weight-semibold">Add</button>
            <button wire:click.prevent="store()" class="btn font-weight-semibold">Save</button>
        </form>
    </div>
    <div class="container-admin container-list-home">
    <h5>Sliders on website</h5>
        @forelse ($all_homepage as $home)
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="contacts__form__one">
                        <h6 class="title m-b-15">Your Sliders: </h6>
                        <div class="infos">
                            <div class="heading color-title m-b-10">
                                <h5>{{$home->slider__heading}}</h5>
                            </div>
                            <div class="sub-heading m-b-10">
                                <span>{{$home->slider__subheading}}</span>
                            </div>
                            <div class="image m-b-10">
                                <img class="img-fluid" style="height:171px;" src="{{url($home->slider__image)}}">
                            </div>
                            <div class="buttons-inline">
                                <button class="btn btn-warning"><a href="{{route('change_sliders.edit',$home->id)}}">Edit</a></button>
                                <form action="{{route('change_sliders.destroy',$home->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <section>
                                        <button class=" pin btn btn-hover font-weight-semibold btn-danger" type="submit">Delete</button>
                                    </section>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @empty
        <p>No fuck</p>
    @endforelse
    </div>
</div>
