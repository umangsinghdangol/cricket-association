<footer class="footer">
    <div class="footer__logo">
      <img src="./images/Cricket_Association_of_Nepal_logo.svg" alt="" />
    </div>

    <div class="footer__links">
      <li><a href="/">Home</a></li>
      <li><a href="/about">About</a></li>
      <li><a href="/programs">Programs</a></li>
      <li><a href="/classes">Classes</a></li>
      <li><a href="/contact">Contact</a></li>
      <!-- <li><a href="#">Donation</a></li> -->
    </div>
    <div class="footer__buttons">
      <div class="subscribe">
        <h6 class="heading">Subscribe to newsletter</h6>
        <label for="email" class="caption">Email</label><br/>
       <form action="{{route('subscriber.store')}}" method="POST">
           @csrf
           <input type="email" name="email_id" placeholder="Enter Your Email" />
           <input type="submit" value="send"/>
       </form>
      </div>

      <div class="social-icons">
        <h6>follow us on:</h6>
        <a href="{{$all_contact->link__facebook}}" target="_blank"><i class="fab fa-facebook"></i></a>
        <a href="{{$all_contact->link__instagram}}" target="_blank"><i class="fab fa-instagram"></i></a>
        <a href="{{$all_contact->link__youtube}}" target="_blank"><i class="fab fa-youtube"></i></a>
      </div>
    </div>
    <h6 class="copyright">Copyright &copy;2020. All Rights Reserved</h6>
  </footer>
