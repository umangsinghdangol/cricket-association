@extends('welcome')
<div class="container programs-grid">
    @include('pages.header')
    <section class="programs_info">
        <div class="programs_info__intro">
            {{-- {{dd($find_programs)}} --}}
            <h4><span>{{$find_programs->program__heading}}</span></h4>
        </div>
        <div class="programs_info__image">
            <img src="{{url($find_programs->program__image)}}" alt="{{$find_programs->program__heading}}" />
        </div>
        <div class="programs_info__description">
            <p>
                {{$find_programs->program__brief}}
            </p>
        </div>
    </section>
    @include('pages.footor')
</div>