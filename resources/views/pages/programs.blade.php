@extends('welcome')
@section('content')

<div class="container container-grid">

@include('pages.header')
  <section class="programs">
  <div class="programs__intro">
    <h4><span>Programs</span></h4>
  </div>
  @forelse ($all_programs as $all_program)
  <div class="programs__tour">
    <div class="img">
    <img src="{{url($all_program->program__image)}}" alt="vision" />
        </div>
        <div class="tour-content">
          <h5>{{$all_program->program__heading}}</h5>
          <p>
            {{$all_program->program__brief}}
          </p>
          <a href="/programsinfo/{{$all_program->id}}">Read More</a>
        </div>
      </div>
      @empty
    <div class="row">
    <section class="form-group m-b-10">
        <p class="messagep m-b-10">Sorry there is no inquiry present....</p>
    </section>
</div>
@endforelse
  </section>
@include('pages.footor')
</div>
@endsection
