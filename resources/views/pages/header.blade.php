<nav class="navbar">
    <div class="navbar__logo">
      <img
        src="{{asset("/images/Cricket_Association_of_Nepal_logo.svg")}}"
        alt="logo"
        class="logo__pic"
      />
    </div>

    <div class="navbar__wrapper">
      <ul class="navbar__links">
        <span class="navbar__list" id="navbar-close">
          <i class="fas fa-times" onclick="closeNav()"></i>

          <li class="navbar__items"><a href="/">Home</a></li>
          <li class="navbar__items"><a href="/about">About</a></li>
          <li class="navbar__items">
            <a href="/programs">Programs</a>
          </li>
          <li class="navbar__items"><a href="/classes">Classes</a></li>
          <li class="navbar__items">
            <a href="/contact">Contact</a>
          </li>
        </span>
        <li class="togglemenu">
          <span class="navbar-toggle" id="navbar-close">
            <i class="fas fa-bars" onclick="openNav()"></i>
          </span>
        </li>
      </ul>
    </div>
     <!-- <div class="navbar__donate">
            <a href="gallery.html"><button>Gallery</button></a>
          </div> -->
  </nav>
