@extends('welcome')
@section('content')
<div class="container container-grid_show">
  @include('pages.header')
  <section class="about-us">
    <div class="about-us__intro">
      <h4><span>About Us</span></h4>
      <p>
        {{$all_about->about__content}}
      </p>
    </div>

    <div class="about-us__vision">
      <div class="img">
        <img src="{{url($all_about->vision__image)}}" alt="vision" />
      </div>
      <div class="vision-content">
        <h4>Vision</h4>
        <p>
          {{$all_about->vision__content}}
        </p>
      </div>
    </div>

    <div class="about-us__mission">
      <div class="img">
      <img src="{{url($all_about->mission__image)}}" alt="mission" />
      </div>
      <div class="mission-content">
        <h4>Mission</h4>
        <p>
          {{$all_about->mission__content}}
        </p>
      </div>
    </div>
  </section>
  @include('pages.footor')
  
</div>
@endsection