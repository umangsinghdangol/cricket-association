@extends('welcome')
@section('content')

<div class="container contact-grid">
    @include('pages.header')

    <section class="contacts">
        <div class="contacts__intro">
            <h4><span>Contact Us</span></h4>
        </div>

        <div class="contacts__form">
            <div class="contacts__form__one">
                <h6 class="title">Get in touch</h6>
                <div class="infos">
                    <div class="email">
                        <i class="fas fa-envelope"></i><span>{{$all_contact->email}}</span>
                </div>
                <div class="location">
                    <i class="fas fa-map-marker-alt"></i
                ><span>{{$all_contact->location}}</span>
            </div>
            <div class="phone">
                <i class="fas fa-phone-alt"></i><span>{{$all_contact->phone_number}}</span>
            </div>
            <div class="follow">
                <h6>Follow us on:</h6>
                <a href="{{$all_contact->link__facebook}}" target="_blank"><i class="fab fa-facebook" style="color: #15a2fa"></i> </a>
                <a href="{{$all_contact->link__instagram}}" target="_blank">
                    <i
                class="fab fa-instagram"
                style="
                    background: radial-gradient(
                        circle at 30% 107%,
                        #fdf497 0%,
                        #fdf497 5%,
                        #fd5949 45%,
                        #d6249f 60%,
                        #285aeb 90%
                        );
                    color: #fff;
                    width: 26px;
                    "
                ></i>
                </a>
                <a href="{{$all_contact->link__youtube}}" target="_blank">
                <i class="fab fa-youtube" style="color: red"></i>
                </a>
            </div>
        </div>
            </div>

            <div class="contacts__form__two">
                <h6>Message Us</h6>
                <form action="{{route("getintouch.store")}}" method="POST">
                    @csrf
                <label> Name </label><br />
                <input type="text" name="u_name" autofocus/><br />
                @error('u_name') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror
                
                <label>Email </label><br />
                <input type="email" name="email" /><br />
                @error('email') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                <label>Message</label><br />
                <textarea name="messages"></textarea><br />
                @error('messages') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                <input type="submit" value="send" />
            </form>
        </div>
    </div>
</section>
<section class="map">
    <iframe
    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d56516.276892053545!2d85.2911131742118!3d27.709031933574085!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb198a307baabf%3A0xb5137c1bf18db1ea!2sKathmandu%2044600!5e0!3m2!1sen!2snp!4v1605890908206!5m2!1sen!2snp"
        width="100%"
        height="370px"
        frameborder="0"
        style="border: 0"
        allowfullscreen=""
        aria-hidden="false"
        tabindex="0"
        ></iframe>
    </section>
@include('pages.footor')
</div>
@endsection
