@extends('welcome')
@section('content')

<div class="container">
  @include('pages.header')
  <section class="slider">
    <div class="swiper-container">
      <div class="swiper-wrapper">
        @forelse($all_homes as $all_home)
          <div class="swiper-slide">
            <div class="bg" style="background-image:url({{url($all_home->slider__image)}});">
              <div class="bg-shadow">
                <div class="slider__content">
                  <h2>{{$all_home->slider__heading}}</h2>
                  <h6>{{$all_home->slider__subheading}}</h6>
                  <a href="/contact">Get in touch</a>
                </div>
                <div class="slider__icons">
                  <h5>Follow us on:</h5>
                  <a href="{{$all_contact->link__facebook}}" target="_blank"><i class="fab fa-facebook"></i></a>
                  <a href="{{$all_contact->link__instagram}}" target="_blank"><i class="fab fa-instagram-square"></i></a>
                  <a href="{{$all_contact->link__youtube}}" target="_blank"><i class="fab fa-youtube"></i></a>
                </div>
              </div>
            </div>
          </div>
          @empty
          <div class="row">
          <section class="form-group m-b-10">
              <p class="messagep m-b-10">Sorry there is no inquiry present....</p>
          </section>
      </div>
      @endforelse
      </div>
    <div class="swiper-button-prev swiper-button-white"></div>
    <div class="swiper-button-next swiper-button-white"></div>
  </section>
</div>
@endsection
