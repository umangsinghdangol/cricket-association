@extends('welcome')
@section('content')
<div class="container class-grid">
  @include('pages.header')
  <section class="classes">
    <div class="classes__intro">
      <h4><span>Classes</span></h4>
    </div>

    <div class="classes__tables">
      <div class="classes__tables__tr table__tr-color">
        <div class="classes__tables__th">
          <strong>Trainers</strong>
        </div>
        <div class="classes__tables__th">
          <strong>Time</strong>
        </div>
        <div class="classes__tables__th">
          <strong>Fee Structure</strong>
        </div>
      </div>
      @forelse ($all_classSection as $all_class)
      <div class="classes__tables__tr">
        <div class="classes__tables__td" title="{{$all_class->trainer}}">{{$all_class->trainer}}</div>
        <div class="classes__tables__td">{{date('h:i A',strtotime($all_class->time__start))}} to {{date('h:i A',strtotime($all_class->time__end))}}</div>
        <div class="classes__tables__td">Rs. {{$all_class->fee_structure}}</div>
      </div>
      @empty
        <div class="row">
        <section class="form-group m-b-10">
            <p class="messagep m-b-10">Sorry there is no inquiry present....</p>
        </section>
      </div>
      @endforelse
    </div>
  </section>
  @include('pages.footor')
</div>
@endsection
