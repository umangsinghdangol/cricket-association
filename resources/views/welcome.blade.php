<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cricket Excellence Center</title>
        <link rel="stylesheet" href="{{mix('/css/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('/fontawesome/fontawesome-free/css/all.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('/swiper/swiper-bundle.min.css')}}">
        
    </head>
    <body>
        @yield('content')
    </body>
    <script>
        function openNav() {
        console.log("hellow");
        document.getElementById("navbar-close").style.width = "100vw";
        }

        function closeNav() {
        document.getElementById("navbar-close").style.width = "0";
        }
    </script>
    
    <script src="{{asset('/swiper/swiper-bundle.min.js')}}"></script>
    
    <script>
       var swiper = new Swiper('.swiper-container', {
        loop:true,
        speed:5000,
        effect:'fade',

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    autoplay: {
        delay: 3000,
    },
    fadeEffect: {
        crossFade: true,
    },
    breakpoints: {
            600: {
            slidesPerView: 1,
            },
            767: {
            slidesPerView: 1,
            },
            1024: {
            slidesPerView: 1,
            },
        }
    });
    </script>
</html>
