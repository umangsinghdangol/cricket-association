<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cricket Excellence Center</title>
        <link rel="stylesheet" href="{{mix('/css/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('/fontawesome/fontawesome-free/css/all.css')}}">
    </head>
    <body class="mainContainer">
        @yield('content')
    </body>
</html>
