<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Forgot password</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="{{mix('/css/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('/fontawesome/fontawesome-free/css/all.css')}}">
    </head>
<body>
    <div class="modal">
        <div class="modal-dialog modal-login">
            <div class="modal-content">
                <div class="modal-header">				
                    <h4 class="modal-title">Forgot Password</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input id="email" class="block mt-1 w-full form-control" type="email" name="email" :value="old('email')" required autofocus placeholder="email"/> 
                            </div>
                            @error('email')
                            <label class="m-b-10" style="color: red">{{$message}}</label>
                        @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block btn-lg">Email Password Reset Link</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>     
</body>
</html>


