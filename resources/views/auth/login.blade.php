<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="{{mix('/css/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('/fontawesome/fontawesome-free/css/all.css')}}">
    </head>
<body>
    <div class="modal">
        <div class="modal-dialog modal-login">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Log In</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input id="email" class="block mt-1 w-full form-control" type="email" name="email" :value="old('email')" required autofocus> <br>
                            </div>
                            @error('email')
                                <label class="m-b-10" style="color: red">{{$message}}</label>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input id="password" class="block mt-1 w-full form-control" type="password" name="password" required autocomplete="current-password"><br>
                            </div>
                            @error('password')
                                 <label class="m-b-10" style="color: red">{{$message}}</label>
                             @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block btn-lg">Log In</button>
                        </div>
                        <p class="hint-text"><a href="{{ route('password.request') }}">Forgot Password?</a></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
