@extends('admin')
@section('content')
@include('auth.bars.header')
@include('auth.bars.navbar')
<div class="page-container">
    <div class="page-header">
        <div class="text-content">
            <h2 class="header-title">Classes</h2>
            <div class="header-sub-title">
                <nav class="breadcrumb breadcrumb-dash">
                    <a href="#" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Home</a>
                    <span class="breadcrumb-item active">Classes</span>
                </nav>
            </div>
        </div>
    </div>

    <div class=" container-admin container-add">
        <form action="{{route('change_class_section.store')}}" method="POST" >
            @csrf
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="trainer_name">Trainer</label>
                            <input  id="trainer_name" name="trainer_name" type="text" autofocus />
                            @error('trainer_name') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="time_start_at">Time start</label>
                            <input  id="time_start_at" name="time_start_at" type="time" />
                            @error('time_start_at') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="time_end_at">Time end</label>
                            <input id="time_end_at" name="time_end_at"  type="time" />
                            @error('time_end_at') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="fee_structure_for_trainer">Fee Structure</label>
                            <input id="fee_structure_for_trainer" name="fee_structure_for_trainer" min="0.00"  step="0.01"type="number" />
                            @error('fee_structure_for_trainer') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                        <button class="btn btn-hover font-weight-semibold" type="submit">Create</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="container-admin container-list">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="classes__tables">
                            <div class="classes__tables__tr table__tr-color">
                                <div class="classes__tables__th">
                                    <strong>Trainers</strong>
                                </div>
                                <div class="classes__tables__th">
                                    <strong>Time</strong>
                                </div>
                                <div class="classes__tables__th">
                                    <strong>Fee Structure</strong>
                                </div>
                                <div class="classes__tables__th">
                                    {{-- <strong>Edit</strong> --}}
                                </div>
                                <div class="classes__tables__th">
                                    {{-- <strong>Delete</strong> --}}
                                </div>
                            </div>
                            @forelse($all_classSection as $classes)
                            <div class="classes__tables__tr">
                                <div class="classes__tables__td" title="{{$classes->trainer}}">{{$classes->trainer}}</div>
                                <div class="classes__tables__td">{{date('h:i A',strtotime($classes->time__start))}} to {{date('h:i A',strtotime($classes->time__end))}}</div>
                                <div class="classes__tables__td">{{$classes->fee_structure}}</div>
                                <div class="classes__tables__td">
                                    <a href="{{route('change_class_section.edit',$classes->id)}}"><button class="btn btn-warning">Edit</button></a>
                                </div>
                                <div class="classes__tables__td">
                                    <form action="{{route('change_class_section.destroy',$classes->id)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <section>
                                            <button class="btn btn-danger">Delete</button>
                                        </section>
                                    </form>
                                </div>
                            </div>
                            @empty
                            <div class="row">
                            <section class="form-group m-b-10">
                                <p class="messagep m-b-10">Sorry there is no inquiry present....</p>
                            </section>
                            @endforelse
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
