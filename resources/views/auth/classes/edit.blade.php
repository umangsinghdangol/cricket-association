@extends('for_edit')
@section('content')
@include('auth.bars.header')
@include('auth.bars.navbar')
<div class="page-container-edit">
    <div class="page-header-edit">
        <div class="text-content">
            <h2 class="header-title">Classes</h2>
            <div class="header-sub-title">
                <nav class="breadcrumb breadcrumb-dash">
                    <a href="#" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Home</a>
                    <a class="breadcrumb-item" href="#">Classes</a>
                    <span class="breadcrumb-item active">Edit</span>
                </nav>
            </div>
        </div>
    </div>
    <div class="container-admin container-edit">
        <div class="tab-content m-t-15">
            <div class="tab-pane fade show active" id="tab-account" >
            <form action="{{route('change_class_section.update',$specific_classSection->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <section class="form-group m-b-10">
                                <label class="m-b-10 color-title font-weight-semibold" for="trainer_name">Trainer</label>
                                <input  id="trainer_name" name="trainer_name" value="{{$specific_classSection->trainer}}" type="text" autofocus />
                                @error('trainer_name') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                            </section>
                            <section class="form-group m-b-10">
                                <label class="m-b-10 color-title font-weight-semibold" for="time_start_at">Time start</label>
                                <input  id="time_start_at" name="time_start_at" value="{{$specific_classSection->time__start}}" type="time" />
                                @error('time_start_at') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                            </section>
                            <section class="form-group m-b-10">
                                <label class="m-b-10 color-title font-weight-semibold" for="time_end_at">Time end</label>
                                <input id="time_end_at" name="time_end_at" value="{{$specific_classSection->time__end}}" type="time" />
                                @error('time_end_at') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                            </section>
                            <section class="form-group m-b-10">
                                <label class="m-b-10 color-title font-weight-semibold" for="fee_structure_for_trainer">Fee Structure</label>
                                <input id="fee_structure_for_trainer" name="fee_structure_for_trainer" value="{{$specific_classSection->fee_structure}}" min="0.00"  step="0.01"type="number" />
                                @error('fee_structure_for_trainer') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                            </section>
                            <input class="btn btn-hover font-weight-semibold " value="Update" type="submit">
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection