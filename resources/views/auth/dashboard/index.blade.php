@extends('admin')
@section('content')
@include('auth.bars.header')
@include('auth.bars.navbar')
<div class="page-container">
    <div class="page-header">
        <div class="text-content">
            <h2 class="header-title">Dashboard</h2>
            <div class="header-sub-title">
                <nav class="breadcrumb breadcrumb-dash">
                    <span class="breadcrumb-item active">Home</span>
                </nav>
            </div>
        </div>
    </div>
    <div class="container-admin container-list">
        <h5>Message Us</h5>
            <div class="card">
                <div class="card-body">
                    @forelse($getInTouch_Info as $getintouch)
                        <div class="row">
                            <section class="form-group m-b-10">
                                <h6 class="m-b-10 color-title font-weight-semibold">{{$getintouch->name}}  <label style="font-size: 14px;">{{$getintouch->email_id}}</label></h6>
                                <p class="messagep m-b-10">{{$getintouch->message}}</p>
                            </section>
                        </div>
                        @empty
                        <div class="row">
                        <section class="form-group m-b-10">
                            <p class="messagep m-b-10">Sorry there is no inquiry present....</p>
                        </section>
                    </div>
                    @endforelse

                </div>
            </div>
    </div>
    <div class="container-admin container-add">
        <h5>Subscribion emails</h5>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            @forelse($subscriber_email as $email)
                                <label class="m-b-10">{{$email->subscriber_emailId}}</label><br>
                            @empty
                                <label class="m-b-10">No Subscriber right now...</label>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
