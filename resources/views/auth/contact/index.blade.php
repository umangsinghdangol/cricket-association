@extends('admin')
@section('content')
@include('auth.bars.header')
@include('auth.bars.navbar')
<div class="page-container">
    <div class="page-header">
        <div class="text-content">
            <h2 class="header-title">Contact</h2>
            <div class="header-sub-title">
                <nav class="breadcrumb breadcrumb-dash">
                    <a href="#" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Home</a>
                    <span class="breadcrumb-item active">Contact</span>
                </nav>
            </div>
        </div>
    </div>
    <div class="container-admin container-add">
        <form action="{{route('change_contact.update',$all_contact->id)}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="get__email">Email</label><br/>
                            <input  id="get__email" name="get__email" value="{{$all_contact->email}}" type="email" autofocus />
                            @error('get__email') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold"for="get__location">Location</label>
                            <input  id="get__location" name="get__location" value="{{$all_contact->location}}" type="text" />
                            @error('get__location') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="get__phone">Phone Number</label>
                            <input id="get__phone" name="get__phone" value="{{$all_contact->phone_number}}" pattern="[0-9]{10}" type="tel" />
                            @error('get__phone') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="get__facebook_link">Facebook Link</label>
                            <input id="get__facebook_link" name="get__facebook_link" value="{{$all_contact->link__facebook}}" placeholder="https://example.com"
                            pattern="https://.*"type="url"/>
                            @error('get__facebook_link') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="get__instagram_link">Instragram Link</label>
                            <input id="get__instagram_link" name="get__instagram_link" value="{{$all_contact->link__instagram}}" placeholder="https://example.com"
                            pattern="https://.*"type="url"/>
                            @error('get__instagram_link') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="get__youtube_link">Youtube Link</label>
                            <input id="get__youtube_link" name="get__youtube_link" value="{{$all_contact->link__youtube}}" placeholder="https://example.com"
                            pattern="https://.*"type="url"/>
                            @error('get__youtube_link') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                    </div>
                    <input class="btn btn-hover font-weight-semibold " value="Update" type="submit">
                </div>
            </div>
        </form>
    </div>
    <div class="container-admin container-list">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="contacts__form__one">
                        <h6 class="title m-b-15">Your Details: </h6>
                        <div class="infos">
                            <div class="email m-b-40">
                                <i class="fas fa-envelope"></i><span>{{$all_contact->email}}</span>
                            </div>
                            <div class="location m-b-40">
                                <i class="fas fa-map-marker-alt"></i>
                                <span>{{$all_contact->location}}</span>
                            </div>
                            <div class="phone m-b-40">
                                <i class="fas fa-phone-alt"></i><span>+977-{{$all_contact->phone_number}}</span>
                            </div>
                            <div class="follow m-b-40">
                                <h6>Follow us on:</h6>
                                <a href="{{$all_contact->link__facebook}}" target="_blank"><i class="fab fa-facebook" style="color: #15a2fa"></i> </a>
                                <a href="{{$all_contact->link__instagram}}" target="_blank">
                                    <i
                                class="fab fa-instagram"
                                style="
                                    background: radial-gradient(
                                        circle at 30% 107%,
                                        #fdf497 0%,
                                        #fdf497 5%,
                                        #fd5949 45%,
                                        #d6249f 60%,
                                        #285aeb 90%
                                        );
                                    color: #fff;
                                    width: 26px;
                                    "
                                ></i>
                                </a>
                                <a href="{{$all_contact->link__youtube}}" target="_blank">
                                <i class="fab fa-youtube" style="color: red"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
