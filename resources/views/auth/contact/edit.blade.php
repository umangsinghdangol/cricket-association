@extends('admin')
@section('content')
@include('auth.bars.header')
@include('auth.bars.navbar')
    <h3>This is Contact page.</h3>

    <br><br><br><br><br>
    <form action="{{route('change_contact.update',$specific_contact->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <section>
            <label for="get__email">Email</label>
            <input  id="get__email" name="get__email" value="{{$specific_contact->email}}" type="email" autofocus />
            @error('get__email') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

        </section>
        <section>
            <label for="get__location">Location</label>
            <input  id="get__location" name="get__location" value="{{$specific_contact->location}}" type="text" />
            @error('get__location') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

        </section>
        <section>
            <label for="get__phone">Phone Number</label>
            <input  id="get__phone" name="get__phone" value="{{$specific_contact->phone_number}}" pattern="[0-9]{10}" type="tel" />
            @error('get__phone') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

        </section>
        <section>
            <label for="get__facebook_link">Facebook Link</label>
            <input  id="get__facebook_link" name="get__facebook_link" value="{{$specific_contact->link__facebook}}" placeholder="https://example.com"
                    pattern="https://.*" type="url" />
                    @error('get__facebook_link') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

        </section>
        <section>
            <label for="get__instagram_link">Instagram Link</label>
            <input  id="get__instagram_link" name="get__instagram_link" value="{{$specific_contact->link__instagram}}" placeholder="https://example.com"
                    pattern="https://.*" type="url" />
                    @error('get__instagram_link') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

        </section>
        <section>
            <label for="get__youtube_link">Youtube Link</label>
            <input  id="get__youtube_link" name="get__youtube_link" value="{{$specific_contact->link__youtube}}" placeholder="https://example.com"
                    pattern="https://.*" type="url" />
                    @error('get__youtube_link') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

        </section>

        <button type="submit">Create</button>
    </form>
@endsection