<div class="side-nav">
    <ul class="side-nav-menu">
        <li class="nav-item dropdown">
            <a class="dropdown-toggle" href="/admin">
                <span class="icon-holder">
                    <i class="fas fa-home"></i>
                </span>
                <span class="title title-nav">Dashboard</span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="dropdown-toggle" href="/admin/change_sliders">
                <span class="icon-holder">
                    <i class="far fa-images"></i>
                </span>
                <span class="title title-nav">Sliders</span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="dropdown-toggle" href="/admin/change_about">
                <span class="icon-holder">
                    <i class="far fa-address-card"></i>
                </span>
                <span class="title title-nav">About Us</span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="dropdown-toggle" href="{{route('change_program.index')}}">
                <span class="icon-holder">
                    <i class="fas fa-tasks"></i>
                </span>
                <span class="title title-nav">Programs</span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="dropdown-toggle" href="/admin/change_class_section">
                <span class="icon-holder">
                    <i class="fas fa-graduation-cap"></i>
                </span>
                <span class="title title-nav">Classes</span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="dropdown-toggle" href="/admin/change_contact">
                <span class="icon-holder">
                    <i class="fas fa-id-card"></i>
                </span>
                <span class="title title-nav">Contact</span>
            </a>
        </li>

    </ul>
</div>
