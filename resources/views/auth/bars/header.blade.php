<div class="header">
    <div class="logo">
        <a href="/admin/change_homepage">
            <img class="header-img" src="{{asset("/images/Cricket_Association_of_Nepal_logo.svg")}}" alt="Logo">
        </a>
    </div>
    <div class="nav-wrap">
       <div class="logout-admin">
        <form action="{{ route('logout') }}" method="post">
            @csrf
            <button class="btn font-weight-semibold"><span class="title">Logout</span></button>
        </form>
       </div>
    </div>
</div>


