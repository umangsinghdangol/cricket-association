@extends('admin')
@section('content')
@include('auth.bars.header')
@include('auth.bars.navbar')
<div class="page-container">
    <div class="page-header">
        <div class="text-content">
            <h2 class="header-title">About Us</h2>
            <div class="header-sub-title">
                <nav class="breadcrumb breadcrumb-dash">
                    <a href="#" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Home</a>
                    <span class="breadcrumb-item active">About Us</span>
                </nav>
            </div>
        </div>
    </div>
    <div class="container-admin container-add">
    <h5> About Edit</h5>
        <form action="{{route('change_about.update',$all_about->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="aboutUsContent">About Content</label>
                            <textarea style="width:100%" id="aboutUsContent" name="aboutUsContent" type="text" autofocus>{{$all_about->about__content}}</textarea>
                            @error('aboutUsContent') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror
                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="visionSectionContent">Vision Content</label>
                            <textarea style="width:100%" id="visionSectionContent" name="visionSectionContent" type="text">{{$all_about->vision__content}}</textarea>
                            @error('visionSectionContent') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror
                            
                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="v_image">Vision Image</label>
                            <input id="v_image" name="v_image" type="file">
                            @error('v_image') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror
                            
                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="missionSectionContent">Mission Content</label>
                            <textarea style="width:100%" id="missionSectionContent" name="missionSectionContent"  type="text">{{$all_about->mission__content}}</textarea>
                            @error('missionSectionContent') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror
                            
                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="m_image">Mission Image</label>
                            <input id="m_image" name="m_image" type="file">
                            @error('m_image') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror
                            
                        </section>
                        <input class="btn btn-hover font-weight-semibold" value="Update" type="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="container-admin container-list">
        <h5>Preview</h5>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="m-b-10 color-title">Main</h5>
                            <p class="m-b-20 col-md-12">{{$all_about->about__content}}</p>

                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <img class="img-fluid" src="{{url($all_about->vision__image)}}" alt="image">
                        </div>
                        <div class="col-md-8">
                            <h5 class="m-b-10 color-title">Vision</h5>
                            <p class="m-b-20 col-md-12">{{$all_about->vision__content}}vision</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">
                            <h5 class="m-b-10 color-title">Mission</h5>
                            <p class="m-b-20 col-md-12">{{$all_about->mission__content}} mission</p>
                        </div>
                        <div class="col-md-4">
                            <img class="img-fluid" src="{{url($all_about->mission__image)}}" alt="image">
                        </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
</div>
@endsection
