@extends('for_edit')
@section('content')
@include('auth.bars.header')
@include('auth.bars.navbar')
<div class="page-container-edit">
    <div class="page-header-edit">
        <div class="text-content">
                <h2 class="header-title">Home</h2>
                <div class="header-sub-title">
                    <nav class="breadcrumb breadcrumb-dash">
                        <a href="#" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Home</a>
                        <span class="breadcrumb-item active">Edit</span>
                    </nav>
                </div>
            </div>
    </div>
    <div class="container-admin container-edit">
        <form action="{{route('change_sliders.update',$specific_homepage->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="p_heading">Heading</label>
                            <textarea style="width:100%" id="get__heading" name="get__heading"  type="text" autofocus>{{$specific_homepage->slider__heading}}</textarea>
                            @error('get__heading') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="p__brief">Sub-Heading</label>
                            <textarea style="width:100%" id="get__subheading" name="get__subheading"  type="text">{{$specific_homepage->slider__subheading}}</textarea>
                            @error('get__subheading') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="p__image">Upload Image</label>
                            <input id="get__image" name="get__image" type="file">
                            {{-- @error('get__image') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror --}}

                        </section>
                        <input class="btn btn-hover font-weight-semibold " value="Update" type="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
