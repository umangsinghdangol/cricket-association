@extends('for_edit')
@section('content')
@include('auth.bars.header')
@include('auth.bars.navbar')
<div class="page-container-edit">
    <div class="page-header-edit">
        <div class="text-content">
                <h2 class="header-title">Programs</h2>
                <div class="header-sub-title">
                    <nav class="breadcrumb breadcrumb-dash">
                        <a href="#" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Home</a>
                        <a class="breadcrumb-item" href="#">Programs</a>
                        <span class="breadcrumb-item active">Edit</span>
                    </nav>
                </div>
            </div>
    </div>
    <div class="container-admin container-edit">
        <form action="{{route('change_program.update',$specific_program->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="p_heading">Heading</label>
                            <textarea style="width:100%" id="p_heading" name="p_heading" value="" type="text" autofocus>{{$specific_program->program__heading}}</textarea>
                            @error('p_heading') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="p__brief">Sub-Heading</label>
                            <textarea style="width:100%" id="p__brief" name="p__brief" value="" type="text">{{$specific_program->program__brief}}</textarea>
                            @error('p__brief') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="p__image">Upload Image</label>
                            <input id="p__image" name="p__image" type="file">
                            {{-- @error('p_heading') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror --}}

                        </section>
                        <input class="btn btn-hover font-weight-semibold " value="Update" type="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection