@extends('admin')
@section('content')
@include('auth.bars.header')
@include('auth.bars.navbar')
<div class="page-container">
   
    <div class="page-header">
        <div class="text-content">
            <h2 class="header-title">Program</h2>
            <div class="header-sub-title">
                <nav class="breadcrumb breadcrumb-dash">
                    <a href="#" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Home</a>
                    <span class="breadcrumb-item active">Programs</span>
                </nav>
            </div>
        </div>
    </div>
    <div class="container-admin container-add">
        <form action="{{--{{route('change_program.store')}}--}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="p_heading">Heading</label>
                            <textarea style="width:100%" id="p_heading" name="p_heading" type="text" autofocus></textarea>
                            @error('p_heading') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="p__brief">Sub-Heading</label>
                            <textarea style="width:100%" id="p__brief" name="p__brief" type="text"></textarea>
                            @error('p__brief') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror

                        </section>
                        <section class="form-group m-b-10">
                            <label class="m-b-10 color-title font-weight-semibold" for="p__image">Upload Image</label>
                            <input id="p__image" name="p__image" type="file">
                            {{-- @error('p__image') <label class="m-b-10" style="color: red">{{$message}}</label> @enderror --}}

                        </section>
                        <input class="btn btn-hover font-weight-semibold " value="Create" type="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="container-admin container-list">
        @forelse($all_programs as $program)
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <img class="img-fluid" src="{{url($program->program__image)}}" alt="image">
                    </div>
                    <div class="col-md-8">
                        <h5 class="m-b-10 color-title">{{$program->program__heading}}</h5>
                        
                        <p class="m-b-20 col-md-12">{{$program->program__brief}}</p>
                        <div class="buttons-inline">
                        <a href="{{route('change_program.edit',$program->id)}}">
                            <button class="btn btn-hover font-weight-semibold btn-warning">Edit</button>
                        </a>
                        <form action="{{route('change_program.destroy',$program->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <section>
                                <input class="btn btn-hover font-weight-semibold btn-danger" value="Delete" type="submit">
                            </section>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @empty
        <div class="row">
        <section class="form-group m-b-10">
            <p class="messagep m-b-10">Sorry there is no inquiry present....</p>
        </section>
    </div>
    @endforelse
    </div>
</div>
@endsection