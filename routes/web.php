<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',[\App\Http\Controllers\ShowIndex\ShowIndexController::class,'homepage_index']);
Route::get('/about',[\App\Http\Controllers\ShowIndex\ShowIndexController::class,'about_index']);
Route::get('/programs',[\App\Http\Controllers\ShowIndex\ShowIndexController::class,'programs_index']);
Route::get('/classes',[\App\Http\Controllers\ShowIndex\ShowIndexController::class,'classes_index']);
Route::get('/contact',[\App\Http\Controllers\ShowIndex\ShowIndexController::class,'contact_index'])->name("homepage.contact");
Route::get('/programsinfo/{id}',[\App\Http\Controllers\ShowIndex\ShowIndexController::class,'programs_more']);


Route::post('/subscriber',[\App\Http\Controllers\Subscriber\SubscriberController::class,'store'])->name("subscriber.store");
Route::post('/getintouch',[\App\Http\Controllers\GetInTouch\GetInTouchController::class,'store'])->name("getintouch.store");

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
});
Route::middleware(['auth:sanctum', 'verified'])->get('/register', function () {
    return view('auth.register');
})->name('register');
Route::middleware(['auth:sanctum', 'verified'])->group(function() {
    Route::get('/admin/change_about',[\App\Http\Controllers\About\AboutController::class,'index'])->name("change_about.index");

        //About us routing
    Route::get('/admin/change_about',[\App\Http\Controllers\About\AboutController::class,'index'])->name("change_about.index");
    Route::put('/admin/change_about/{id}',[\App\Http\Controllers\About\AboutController::class,'update'])->name("change_about.update");

    Route::get('/admin/change_contact',[\App\Http\Controllers\Contact\ContactController::class,'index'])->name('change_contact.index');
    Route::put('/admin/change_contact/{id}',[\App\Http\Controllers\Contact\ContactController::class,'update'])->name('change_contact.update');

    Route::resource('/admin/change_program',\App\Http\Controllers\Program\ProgramController::class);



    Route::resource('/admin/change_class_section',\App\Http\Controllers\ClassSection\ClassSectionController::class);
    // Route::resource('/admin/change_class_section',\App\Http\Controllers\ClassSection\ClassSectionController::class,'index');


    Route::get('/admin/change_sliders',function(){
        return view('dashboard');
    })->name('change_sliders.index');

    Route::get('/admin/change_sliders/{id}',[\App\Http\Controllers\Homepage\HomepageController::class,'edit'])->name('change_sliders.edit');
    Route::delete('/admin/change_sliders/{id}',[\App\Http\Controllers\Homepage\HomepageController::class,'destroy'])->name('change_sliders.destroy');
    Route::put('/admin/change_sliders/{id}',[\App\Http\Controllers\Homepage\HomepageController::class,'update'])->name('change_sliders.update');

    Route::resource('/admin',\App\Http\Controllers\Dashboard\DashboardController::class);
    // // Route::get('/admin',\App\Http\Livewire\Homepage::class);
    //for register
});
