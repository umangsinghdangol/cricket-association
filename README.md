<p align="center"><a href="https://www.facebook.com/pailacreation" target="_blank"><img src="https://gitlab.com/uploads/-/system/user/avatar/7390501/avatar.png?width=400" width="400"></a></p>

## About Paila Creation

We are the modern design website,application design company. We like to work very closely with the client and produces fine product with the requirement of the client.

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## We Offer

Web application, Android Application, Desktop Application and every Kind of the IT solution.

## About Cricket Association

Web application used by circket assosciation including the following features.

+ **Deadline**
    + 2 week -> Full website

+ **Pages needed**
    + Header
        + Facebook, instagram, youtube
    + Donate
    + Homepage
    + About us 
        + Vision 
        + Mission
    + Program 
        + Summer Camp
        + International Tour 
        + Tourmaments 
    + Contact us 
    + Classes  
        + Trainer 
        + Name of coach
        + Timing Cricket Academy 
        + Fee structure of Circket Academy

+ **Reference Links**
    + https://www.lords.org/mcc/mcc-foundation
    + https://hdfa.org/

    + **Donation Design**
        + https://viratkohli.foundation/

+ **features**
    + (will be updated once we get it)

## Member Invovled

+ **Wireframe design**
    + Samir Shrestha 
        + Deadline -> 1 day

+ **Designer**
    + Samir Shrestha
        + Deadline -> 2 day

+ **Guidence to Frontend Designer**
    + Kirti Man Singh
    + Vivek

+ **Database Design**
    + No allocated
        + Deadline -> 1 day

+ **Backend**
    + No allocated 
        + Deadline -> 1 week

+ **Error** 
    + Deadline -> 1 day

+ **Test**
    + No allocated
        + Deadline -> 2 days

## To run this project

```cmd 
composer install
npm install
cp .env.example.com .env
php artisan key:generate
php artisan migrate --seed
php artisan storage:link

//incase storage link not work do 
rm public/storage
php artisan storage:link
```

