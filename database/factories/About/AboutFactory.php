<?php

namespace Database\Factories\About;

use App\Models\About\About;
use Illuminate\Database\Eloquent\Factories\Factory;

class AboutFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = About::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'about__content'=>$this->faker->text($maxNbChars = 400) ,
            'vision__content'=>$this->faker->text($maxNbChars = 400) ,
            'vision__image'=>$this->faker->imageUrl($width = 414, $height = 332),
            'mission__content'=>$this->faker->text($maxNbChars = 400) ,
            'mission__image'=>$this->faker->imageUrl($width = 414, $height = 332)
        ];
    }
}
