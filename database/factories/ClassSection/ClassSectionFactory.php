<?php

namespace Database\Factories\ClassSection;

use App\Models\ClassSection\ClassSection;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClassSectionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ClassSection::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'trainer'=>$this->faker->name,
            'time__start'=>$this->faker->time($format = 'H:i:s', $max = 'now'),
            'time__end'=>$this->faker->time($format = 'H:i:s', $max = 'now'),
            'fee_structure'=>$this->faker->numberBetween($min = 1000, $max = 9000)
        ];
    }
}
