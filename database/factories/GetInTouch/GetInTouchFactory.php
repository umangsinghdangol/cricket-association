<?php

namespace Database\Factories\GetInTouch;

use App\Models\GetInTouch\GetInTouch;
use Illuminate\Database\Eloquent\Factories\Factory;

class GetInTouchFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = GetInTouch::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
