<?php

namespace Database\Factories\Homepage;

use App\Models\Homepage\Homepage;
use Illuminate\Database\Eloquent\Factories\Factory;

class HomepageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Homepage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'slider__heading'=>$this->faker->text($maxNbChars = 30),
            'slider__subheading'=>$this->faker->text($maxNbChars = 50),
            'slider__image'=>$this->faker->imageUrl($width = 1440, $height = 1024)
        ];
    }
}
