<?php

namespace Database\Factories\Contact;

use App\Models\Contact\Contact;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'email'=>$this->faker->safeEmail,
            'location'=>$this->faker->address,
            'phone_number'=>$this->faker->e164PhoneNumber,
            'link__facebook'=>$this->faker->url,
            'link__instagram'=>$this->faker->url,
            'link__youtube'=>$this->faker->url
        ];
    }
}
