<?php

namespace Database\Factories\Program;

use App\Models\Program\Program;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProgramFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Program::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
           'program__heading'=>$this->faker->text($maxNbChars = 30),
           'program__brief'=>$this->faker->text($maxNbChars = 50),
           'program__image'=>$this->faker->imageUrl($width = 414, $height = 332)
        ];
    }
}
