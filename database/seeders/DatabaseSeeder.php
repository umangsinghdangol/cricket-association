<?php

namespace Database\Seeders;


use App\Models\About\About;
use App\Models\ClassSection\ClassSection;
use App\Models\Contact\Contact;
use App\Models\Donation\Donation;
use App\Models\Homepage\Homepage;
use App\Models\Program\Program;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         //  \App\Models\User::factory(10)->create();
        About::factory(1)->create();
        ClassSection::factory(1)->create();
        Contact::factory(1)->create();
        Donation::factory(1)->create();
        Homepage::factory(4)->create();
        Program::factory(1)->create();
        $this->call(AdminSeeder::class);
    }
}
