<?php


namespace App\Repository\Donation;


use App\Models\Donation\Donation;

class DonationRepository
{
    /**
     * @var Donation
     */
    private $donation;

    /**
     * DonationRepository constructor.
     * @param Donation $donation
     */
    public function __construct(Donation $donation)
    {
        $this->donation = $donation;
    }
}
