<?php


namespace App\Repository\Additional;


use App\Repository\Image\ImageRepository;

class Converter
{
    /**
     * @var ImageRepository
     */
    private $imageRepository;

    public function __construct(ImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    public function mapAbout($data,$updateOrCreate=0,$previousImageVision='',$previousImageMission=''){

        try{
           if($updateOrCreate==0){
               $vision_image_location= $this->imageRepository->addImages($data->only('v_image'));
               $mission_image_location= $this->imageRepository->addImages($data->only('m_image'));
                $mappedAbout = collect([
                    'about__content' => $data->aboutUsContent,
                    'vision__content' => $data->visionSectionContent,
                    'vision__image' => $vision_image_location,
                    'mission__content' =>$data-> missionSectionContent,
                    'mission__image' => $mission_image_location,
                ]);

            }
           else{
               $vision_image_location= $this->imageRepository->addImages($data->only('v_image'),1);
               $mission_image_location= $this->imageRepository->addImages($data->only('m_image'),1);
               $mappedAbout = collect([
                   'about__content' => $data->aboutUsContent,
                   'vision__content' => $data->visionSectionContent,
                   'mission__content' =>$data-> missionSectionContent,
               ]);
                   if($data->v_image!=false){
                       $mappedAbout->put('vision__image',$vision_image_location);
                       $this->imageRepository->deleteImages($previousImageVision);

                }
                if($data->m_image!=false){
                    $mappedAbout->put('mission__image',$mission_image_location);
                    $this->imageRepository->deleteImages($previousImageMission);
                }
           }
            return $mappedAbout->filter()->toArray();
        }
        catch(\Exception $exception){
            abort( response( 'Something went wrong',500));
        }
    }
    public function mapClassSection($data){
        try{
        $mappedClassSection = collect([
            'trainer' => $data->trainer_name,
            'time__start' => $data->time_start_at,
            'time__end' => $data->time_end_at,
            'fee_structure' =>$data-> fee_structure_for_trainer,
        ]);
        return $mappedClassSection->filter()->toArray();
         }
        catch(\Exception $exception){
            abort( response( 'Something went wrong',500));
        }
    }

    public function mapContact($data){
        try{
            $mappedContact = collect([
                'email' => $data->get__email,
                'location' => $data->get__location,
                'phone_number' => $data->get__phone,
                'link__facebook' =>$data-> get__facebook_link,
                'link__instagram' =>$data->get__instagram_link ,
                'link__youtube' =>$data->get__youtube_link ,
            ]);
            return $mappedContact->filter()->toArray();
        }
        catch(\Exception $exception){
            abort( response( 'Something went wrong',500));
        }
    }
    public function mapHomepage($data,$updateOrCreateHomepage=0,$previousImage=''){
        try{
            if($updateOrCreateHomepage==0){
                if(!array_key_exists('get__image', $data)){
                    $data['get__image']=false;
                }
                $image_location= $this->imageRepository->storeSlider($data['get__image']);

                $mappedHomepage = collect([
                    'slider__heading' => $data['get__heading'],
                    'slider__subheading' => $data['get__subheading'],
                    'slider__image' => $image_location,
                ]);

            }
            else{
                $image_location= $this->imageRepository->addImages($data->only('get__image'),1);
                $mappedHomepage = collect([
                    'slider__heading' => $data->get__heading,
                    'slider__subheading' => $data->get__subheading,
                ]);
                if($image_location!=false){
                    $mappedHomepage->put('slider__image',$image_location);
                    $this->imageRepository->deleteImages($previousImage);
                }
            }

            return $mappedHomepage->filter()->toArray();
        }
        catch(\Exception $exception){
            dump($exception);
            abort( response( 'Something went wrong',500));
        }
    }
    public function mapProgram($data,$updateOrCreateProgram=0,$previousImage=''){
        try{
            if($updateOrCreateProgram==0){
                $image_location= $this->imageRepository->addImages($data->only('p__image'));

                $mappedProgram = collect([
                    'program__heading' => $data->p_heading,
                    'program__brief' => $data->p__brief,
                    'program__image' => $image_location,
                ]);

            }
            else{
                $image_location= $this->imageRepository->addImages($data->only('p__image'),1);
                $mappedProgram = collect([
                    'program__heading' => $data->p_heading,
                    'program__brief' => $data->p__brief,
                ]);
                if($data->p__image!=false){
                    $mappedProgram->put('program__image',$image_location);
                    $this->imageRepository->deleteImages($previousImage);
                }
            }

            return $mappedProgram->filter()->toArray();
        }
        catch(\Exception $exception){
            abort( response( 'Something went wrong',500));
        }
    }
    public function mapDonation(){
        //
    }
    public function mapSubscriber($data){
        try{
            $mappedContact = collect([
                'subscriber_emailId' => $data->email_id,
            ]);
            return $mappedContact->filter()->toArray();
        }
        catch(\Exception $exception){
            abort( response( 'Something went wrong',500));
        }
    }
    public function mapGetInTouch($data){
        try{
            $mappedContact = collect([
                'name' => $data->u_name,
                'email_id' => $data->email,
                'message' => $data->messages,
            ]);
            return $mappedContact->filter()->toArray();
        }
        catch(\Exception $exception){
            abort( response( 'Something went wrong',500));
        }
    }

}
