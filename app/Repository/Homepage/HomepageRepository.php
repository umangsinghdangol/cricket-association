<?php


namespace App\Repository\Homepage;


use App\Models\Homepage\Homepage;
use App\Repository\Additional\Converter;
use App\Repository\Image\ImageRepository;

class HomepageRepository
{
    /**
     * @var Homepage
     */
    private $homepage;
    /**
     * @var ImageRepository
     */
    private ImageRepository $imageRepository;
    /**
     * @var Converter
     */
    private Converter $converter;

    /**
     * HomepageRepository constructor.
     * @param Homepage $homepage
     * @param ImageRepository $imageRepository
     * @param Converter $converter
     */
    public function __construct(Homepage $homepage,ImageRepository $imageRepository,Converter $converter)
    {
        $this->homepage = $homepage;
        $this->imageRepository = $imageRepository;
        $this->converter = $converter;
    }

    public function allHome(){
            try{
        return $this->homepage->orderBy('id')->get();
            }
        catch(\Exception $exception){
            abort( response('Something went inside wrong',500));
        }
    }
    public function createHomepage($data){
        try{
            return $this->homepage->create($this->converter->mapHomepage($data));
        }
        catch(\Exception $exception){
            abort( response('Something went wrong',500));
        }
    }

    public function findHomepage($id){
        try{
           return $this->homepage::find($id);
       }
       catch(\Exception $exception){
           abort( response( 'Something went wrong',500));
        }
   }

    public function updateHomepage($data,$id){
        try{
            $previousImageProgram=$this->homepage::find($id)->slider__image;
            return $this->homepage->where('id','=',$id)->update($this->converter->mapHomepage($data,1,$previousImageProgram));
        }
        catch(\Exception $exception){
            abort( response('Something went wrong inside',500));
        }
    }
    public function deleteHomepage($id){
        try{
            $homepageDelete= $this->homepage::find($id);
            $this->imageRepository->deleteImages($homepageDelete->slider__image);
            $homepageDelete->delete();
        }
        catch(\Exception $exception){
            abort( response( 'Something went wrong',500));
        }
    }

}
