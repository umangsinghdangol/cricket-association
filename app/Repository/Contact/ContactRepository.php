<?php


namespace App\Repository\Contact;


use App\Models\Contact\Contact;
use App\Repository\Additional\Converter;

class ContactRepository
{
    /**
     * @var Contact
     * @var Converter
     */
    private $contact;
    private $converter;

    /**
     * ContactRepository constructor.
     * @param Contact $contact
     * @param Converter $converter
     */
    public function __construct(Contact $contact, Converter $converter)
    {
        $this->contact = $contact;
        $this->converter = $converter;
    }

    /**
     * @return mixed
     * To display all contact details
     */
    public function allContact(){
        try{
        return $this->contact->orderBy('id')->first();
         }
            catch(\Exception $exception){
                abort( response('Something went inside wrong',500));
            }
    }

    /**
     * @param $data
     * @param $id
     * @return mixed
     * to update contact details
     */
    public function updateContact($data, $id){
        try{
        return $this->contact->where('id','=',$id)->update($this->converter->mapContact($data));
         }
            catch(\Exception $exception){
                dd($exception);
                abort( response('Something went inside wrong',500));
            }
    }
}
