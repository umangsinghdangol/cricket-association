<?php


namespace App\Repository\ClassSection;


use App\Models\ClassSection\ClassSection;
use App\Repository\Additional\Converter;

class ClassSectionRepository
{
    /**
     * @var ClassSection
     */
    private $classSection;
    /**
     * @var Converter
     */
    private $converter;

    /**
     * ClassSectionRepository constructor.
     * @param ClassSection $classSection
     */
    public function __construct(ClassSection $classSection, Converter $converter)
    {
        $this->classSection = $classSection;
        $this->converter = $converter;
    }
    public function allClassSection(){
         try{
        return $this->classSection->orderBy('id')->get();
         }
            catch(\Exception $exception){
                abort( response('Something went inside wrong',500));
            }
    }
    public function findClassSection($id){
        try{
            return $this->classSection::find($id);
        }
        catch(\Exception $exception){
            abort( response( 'Something went wrong',500));
        }
    }
    public function createClassSection($data){
         try{
        return $this->classSection->create($this->converter->mapClassSection($data));
         }
            catch(\Exception $exception){
                dd($exception);
                abort( response('Something went inside wrong',500));
            }
    }
    public function updateClassSection($data,$id){
         try{
        return $this->classSection->where('id','=',$id)->update($this->converter->mapClassSection($data));
         }
            catch(\Exception $exception){
                dd($exception);
                abort( response('Something went inside wrong',500));
            }
    }
    public function deleteClassSection($id){
         try{
        return $this->classSection::find($id)->delete();
         }
            catch(\Exception $exception){
                dd($exception);
                abort( response('Something went inside wrong',500));
            }
    }
}
