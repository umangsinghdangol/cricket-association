<?php


namespace App\Repository\About;


use App\Models\About\About;
use App\Repository\Additional\Converter;
use App\Repository\Image\ImageRepository;
use Illuminate\Support\Facades\Storage;

class AboutRepository
{
    /**
     * @var Converter
     * @var ImageRepository
     *  @var About
     */
    private $about;
    private $converter;
    private $imageRepository;

    /**
     * AboutRepository constructor.
     * @param About $about
     * @param Converter $converter
     * @param ImageRepository $imageRepository
     */
    public function __construct(About $about,Converter $converter, ImageRepository $imageRepository)
    {
        $this->about = $about;
        $this->converter = $converter;
        $this->imageRepository = $imageRepository;
    }

    /**
     * @return mixed
     * Display data from the about us.
     */
    public function allAbout(){
         try{
        return $this->about->orderBy('id')->first();
         }
        catch(\Exception $exception){
            abort( response('Something went inside wrong',500));
        }
    }


    /**
     * @param $data
     * @param $id
     * @return mixed
     * Updates data into about section
     */
    public function updateAbout($data, $id){
            try{
                $about= $this->about::find($id);
                $previous_vision=$about->vision__image;
                    $previous_mission=$about->mission__image;
                return $this->about->where('id','=',$id)->update($this->converter->mapAbout($data,1,$previous_vision,$previous_mission));
            }
            catch(\Exception $exception){
                abort( response('Something went wrong inside',500));
            }
    }
}
