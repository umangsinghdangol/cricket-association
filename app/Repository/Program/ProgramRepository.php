<?php


namespace App\Repository\Program;


use App\Models\Program\Program;
use App\Repository\Additional\Converter;
use App\Repository\Image\ImageRepository;
use Illuminate\Support\Facades\Storage;

class ProgramRepository
{
    /**
     * @var Program
     */
    private $program;
    /**
     * @var Converter
     */
    private $converter;
    /**
     * @var ImageRepository
     */
    private $imageRepository;

    /**
     * ProgramRepository constructor.
     * @param Program $program
     */
    public function __construct(Program $program, Converter $converter, ImageRepository $imageRepository)
    {
        $this->program = $program;
        $this->converter = $converter;
        $this->imageRepository = $imageRepository;
    }
    public function allProgram(){
        return $this->program->orderBy('id')->get();
    }
    public function createProgram($data){

            try{
                return $this->program->create($this->converter->mapProgram($data));
            }
            catch(\Exception $exception){
               abort( response('Something went wrong',500));
            }

    }

    public function findProgram($id){
         try{
            return $this->program::find($id);
        }
        catch(\Exception $exception){
            abort( response( 'Something went wrong',500));
         }
    }

    public function updateProgram($data,$id){

             try{
                 $previousImageProgram=$this->program::find($id)->program__image;
               return $this->program->where('id','=',$id)->update($this->converter->mapProgram($data,1,$previousImageProgram));
             }
             catch(\Exception $exception){
                 abort( response('Something went wrong inside',500));
             }
    }
    public function deleteProgram($id){
         try{
             $programDelete= $this->program::find($id);
             $this->imageRepository->deleteImages($programDelete->program__image);
             $programDelete->delete();
         }
        catch(\Exception $exception){
             abort( response( 'Something went wrong',500));
         }
    }
}
