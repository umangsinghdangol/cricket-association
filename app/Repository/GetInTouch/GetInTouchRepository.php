<?php


namespace App\Repository\GetInTouch;


use App\Models\GetInTouch\GetInTouch;
use App\Repository\Additional\Converter;

class GetInTouchRepository
{
    /**
     * @var GetInTouch
     */
    private GetInTouch $getInTouch;
    /**
     * @var Converter
     */
    private Converter $converter;

    /**
     * GetInTouchRepository constructor.
     * @param GetInTouch $getInTouch
     * @param Converter $converter
     */
    public function __construct(GetInTouch $getInTouch,Converter $converter)
    {
        $this->getInTouch = $getInTouch;
        $this->converter = $converter;
    }
    public function allGetInTouch(){
        return $this->getInTouch->orderByDesc('updated_at')->paginate(30);
    }
    public function createGetInTouch($data){

        try{
            return $this->getInTouch->create($this->converter->mapGetInTouch($data));
        }
        catch(\Exception $exception){
            abort( response('Something went wrong',500));
        }
    }

}
