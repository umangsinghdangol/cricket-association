<?php


namespace App\Repository\Image;


use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;



class ImageRepository
{


    public function addImages($request,$update=0)
    {
        if(!empty($request)){
            return $this->storeImage($request[key($request)]);
        }
        else{
            if($update==0){
                $disPath = '/storage/images/noimage.jpg';
                return $disPath;
            }
            else{
                return false;
            }
        }
    }

    public function deleteImages($delete){
        if (strpos($delete, 'noimage') == false) {
            try {
                Storage::delete(str_replace('/storage', '/public', $delete));
            } catch (\Exception $exception) {
                abort(response('Opps no image found', 500));
            }
        }
    }
    public function storeSlider($image,$update=0){
        if($image!=false || $image!=null){
            return $this->storeImage($image);
        }
        else{
            if($update==0){
                $disPath = '/storage/images/noimage.jpg';
                return $disPath;
            }
            else{
                return false;
            }
        }
    }
    private function storeImage($image){
        // GET JUST EXT
        $extension=$image->getClientOriginalExtension();
        // filename to store
        $fileNameToStore=rand().'_'.time().'.'.$extension;
        // upload image
        $image->storeAs('public/images',$fileNameToStore);
        // display path
        $disPath='/storage/images/'.$fileNameToStore;
        return $disPath;
    }


}
