<?php


namespace App\Repository\Subscriber;


use App\Models\Subscriber\Subscriber;
use App\Repository\Additional\Converter;

class SubscriberRepository
{
    /**
     * @var Subscriber
     */
    private Subscriber $subscriber;
    /**
     * @var Converter
     */
    private Converter $converter;

    /**
     * SubscriberRepository constructor.
     * @param Subscriber $subscriber
     */
    public function __construct(Subscriber $subscriber, Converter $converter)
    {
        $this->subscriber = $subscriber;
        $this->converter = $converter;
    }
    public function allSubscriber(){
        return $this->subscriber->orderByDesc('updated_at')->paginate(30);
    }
    public function createSubscriber($data){

        try{
            return $this->subscriber->create($this->converter->mapSubscriber($data));
        }
        catch(\Exception $exception){
            abort( response('Something went wrong',500));
        }
    }
}
