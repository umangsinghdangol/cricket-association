<?php

namespace App\Models\GetInTouch;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GetInTouch extends Model
{
    use HasFactory;
    protected $fillable = ['name','email_id','message'];
}
