<?php

namespace App\Models\ClassSection;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassSection extends Model
{
    use HasFactory;
    protected $fillable = ['trainer','time__start','time__end','fee_structure'];
}
