<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use App\Http\Requests\Homepage\HomepageRequest;
use Illuminate\Http\Request;
use App\Repository\Homepage\HomepageRepository;
use Illuminate\Support\Facades\DB;

class HomepageController extends Controller
{
    /**
     * @var HomepageRepository
     */
    private $homepageRepository;

    /**
     * HomepageController constructor.
     * @param HomepageRepository $homepageRepository
     */
    public function __construct(HomepageRepository $homepageRepository)
    {
        $this->homepageRepository = $homepageRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("auth.home.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HomepageRequest $request)
    {
        $data = collect([
            'slider__heading'=>'Ground of HERO',
            'slider__subheading'=>'Welcome we are Cricket Association',
            'slider__image'=>'public/storage/cover/slider1.jpg',
        ]);
        DB::transaction(function() use($data) {
            $this->homepageRepository->createHomepage($data);
        });
        return"Data successful inserted in Homepage.";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $specific_homepage=$this->homepageRepository->findHomepage($id);
        return view("auth.home.edit",compact('specific_homepage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HomepageRequest $request, $id)
    {
        DB::transaction(function() use($request,$id) {
            $this->homepageRepository->updateHomepage($request,$id);
        });
        return redirect()->route('change_sliders.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function() use($id){
            $this->homepageRepository->deleteHomepage($id);
        });
        return redirect()->route('change_sliders.index');
    }
}
