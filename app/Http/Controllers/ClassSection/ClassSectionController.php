<?php

namespace App\Http\Controllers\ClassSection;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClassSection\ClassSectionRequest;
use App\Repository\ClassSection\ClassSectionRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClassSectionController extends Controller
{
    /**
     * @var ClassSectionRepository
     */
    private $classSectionRepository;

    /**
     * ClassSectionController constructor.
     * @param ClassSectionRepository $classSectionRepository
     */
    public function __construct(ClassSectionRepository $classSectionRepository)
    {
        $this->classSectionRepository = $classSectionRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_classSection = $this->classSectionRepository->allClassSection();
        return view("auth.classes.index",compact('all_classSection'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClassSectionRequest $request)
    {
        DB::transaction(function() use($request) {
            $this->classSectionRepository->createClassSection($request);
        });
        return redirect()->route('change_class_section.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $specific_classSection=$this->classSectionRepository->findClassSection($id);
        return view("auth.classes.edit",compact('specific_classSection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClassSectionRequest $request, $id)
    {
            DB::transaction(function() use($request,$id) {
            $this->classSectionRepository->updateClassSection($request,$id);
        });
        return redirect()->route('change_class_section.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function() use($id){
            $this->classSectionRepository->deleteClassSection($id);
        });
        return redirect()->route('change_class_section.index');
    }
}
