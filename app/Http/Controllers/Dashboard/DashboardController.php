<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Repository\GetInTouch\GetInTouchRepository;
use App\Repository\Subscriber\SubscriberRepository;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    private SubscriberRepository $subscriberRepository;
    /**
     * @var GetInTouchRepository
     */
    private GetInTouchRepository $getInTouchRepository;

    /**
     * SubscriberController constructor.
     * @param SubscriberRepository $subscriberRepository
     * @param GetInTouchRepository $getInTouchRepository
     */
    public function __construct(SubscriberRepository $subscriberRepository,GetInTouchRepository $getInTouchRepository)
    {
        $this->subscriberRepository = $subscriberRepository;
        $this->getInTouchRepository = $getInTouchRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscriber_email=$this->subscriberRepository->allSubscriber();
        $getInTouch_Info=$this->getInTouchRepository->allGetInTouch();
        return view('auth.dashboard.index',compact("subscriber_email",'getInTouch_Info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
