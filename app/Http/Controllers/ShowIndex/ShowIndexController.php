<?php

namespace App\Http\Controllers\ShowIndex;

use App\Http\Controllers\Controller;
use App\Repository\About\AboutRepository;
use App\Repository\ClassSection\ClassSectionRepository;
use App\Repository\Contact\ContactRepository;
use App\Repository\Homepage\HomepageRepository;
use App\Repository\Program\ProgramRepository;

use Illuminate\Http\Request;

class ShowIndexController extends Controller
{

    private $classSectionRepository;
    private $contactRepository;
    private $aboutRepository;
    // private $homepageRepository;
    private $programRepository;

    

    public function __construct(ProgramRepository $programRepository,HomepageRepository $homepageRepository,AboutRepository $aboutRepository, ClassSectionRepository $classSectionRepository,ContactRepository $contactRepository)
    {
        $this->classSectionRepository = $classSectionRepository;
        $this->contactRepository = $contactRepository;
        $this->aboutRepository = $aboutRepository;
        $this->homepageRepository = $homepageRepository;
        $this->programRepository = $programRepository;

    }

    public function about_index()
    {
        $all_contact = $this->contactRepository->allContact();
        $all_about=$this->aboutRepository->allAbout();
        return view("pages.aboutus",compact('all_about','all_contact'));
    }

    public function programs_index()
    {
        $all_programs=$this->programRepository->allProgram();
        $all_contact = $this->contactRepository->allContact();

       return view("pages.programs",compact('all_programs','all_contact'));
    }

    public function programs_more($id)
    {
        $all_contact = $this->contactRepository->allContact();
        $find_programs=$this->programRepository->findProgram($id);
       return view("pages.programsinfo",compact('find_programs','all_contact'));
    }

    public function contact_index()
    {
        $all_contact = $this->contactRepository->allContact();
        return view("pages.contactUs",compact('all_contact'));
    }

    public function homepage_index()
    {
        $all_contact = $this->contactRepository->allContact();
        $all_homes=$this->homepageRepository->allHome();
        return view("pages.landing",compact('all_homes','all_contact'));
    }

    public function classes_index()
    {
        $all_contact = $this->contactRepository->allContact();
        $all_classSection = $this->classSectionRepository->allClassSection();
        return view("pages.classes",compact('all_classSection','all_contact'));
    }

}
