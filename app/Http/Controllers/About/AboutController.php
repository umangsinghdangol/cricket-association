<?php

namespace App\Http\Controllers\About;

use App\Http\Controllers\Controller;
use App\Http\Requests\About\AboutRequest;
use App\Repository\About\AboutRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AboutController extends Controller
{
    /**
     * @var AboutRepository
     */
    private $aboutRepository;

    /**
     * AboutController constructor.
     * @param AboutRepository $aboutRepository
     */
    public function __construct(AboutRepository $aboutRepository)
    {
        $this->aboutRepository = $aboutRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_about=$this->aboutRepository->allAbout();
        return view("auth.aboutus.index",compact('all_about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AboutRequest $request, $id)
    {
        DB::transaction(function() use($request,$id) {
            $this->aboutRepository->updateAbout($request,$id);
        });
        return redirect()->route('change_about.index');
    }
}
