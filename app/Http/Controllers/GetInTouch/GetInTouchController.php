<?php

namespace App\Http\Controllers\GetInTouch;

use App\Http\Controllers\Controller;
use App\Http\Requests\GetInTouch\GetInTouchRequest;
use App\Repository\GetInTouch\GetInTouchRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetInTouchController extends Controller
{
    /**
     * @var GetInTouchRepository
     */
    private GetInTouchRepository $getInTouchRepository;

    /**
     * GetInTouchController constructor.
     * @param GetInTouchRepository $getInTouchRepository
     */
    public function __construct(GetInTouchRepository $getInTouchRepository)
    {
        $this->getInTouchRepository = $getInTouchRepository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GetInTouchRequest $request)
    {
        DB::transaction(function() use($request) {
            $this->getInTouchRepository->createGetInTouch($request);
        });
        return redirect()->route('homepage.contact');
    }

}
