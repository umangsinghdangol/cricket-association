<?php

namespace App\Http\Controllers\Subscriber;

use App\Http\Controllers\Controller;
use App\Http\Requests\Subscriber\SubscriberRequest;
use App\Repository\Subscriber\SubscriberRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubscriberController extends Controller
{
    /**
     * @var SubscriberRepository
     */
    private SubscriberRepository $subscriberRepository;

    /**
     * SubscriberController constructor.
     * @param SubscriberRepository $subscriberRepository
     */
    public function __construct(SubscriberRepository $subscriberRepository)
    {
        $this->subscriberRepository = $subscriberRepository;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubscriberRequest $request)
    {
        DB::transaction(function() use($request) {
            $this->subscriberRepository->createSubscriber($request);
        });
        return redirect()->route('homepage.contact');
    }
}
