<?php

namespace App\Http\Requests\ClassSection;

use Illuminate\Foundation\Http\FormRequest;

class ClassSectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trainer_name' => 'required|regex:/(^([a-zA-z]+)(\d+)?$)/u',
            'time_start_at' => 'required',
            'time_end_at' => 'required',
            'fee_structure_for_trainer' => 'required|integer|between:1,50000',
            
        ];
    }

    public function messages()
    {
        return [
            'trainer_name.required' => 'Please fill in the box',
            'time_start_at.required' => 'Please fill in the box',
            'time_end_at.required' => 'Please fill in the box',
            'fee_structure_for_trainer.required' => 'Please fill in the box',
            
        ];
        
    }
}
