<?php

namespace App\Http\Requests\GetInTouch;

use Illuminate\Foundation\Http\FormRequest;

class GetInTouchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'u_name' => 'required',
            'email' => 'required',
            'messages' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'u_name.required' => 'Please fill in the box',
            'email.required' => 'Please fill in the box',
            'messages.required' => 'Please fill in the box',
        ];
        
    }
}
