<?php

namespace App\Http\Requests\Contact;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'get__email' => 'required|email|unique:users,email',
            'get__location' => 'required',
            'get__phone' => 'required|digits:10',
            'get__facebook_link' => 'required',
            'get__instagram_link' => 'required',
            'get__youtube_link' => 'required',
        ];
    }

    public function messages()
    {
        return [
            
            'get__email.required' => 'Please fill in the box',
            'get__email.email' => 'Please enter valid email address',

            'get__location.required' => 'Please fill in the box',

            'get__phone.required' => 'Please fill in the box',
            'get__phone.digits' => 'Please fill in the box',

            'get__facebook_link.required' => 'Please fill in the box',
            'get__instagram_link.required' => 'Please fill in the box',
            'get__youtube_link.required' => 'Please fill in the box',
        ];
    }
}
