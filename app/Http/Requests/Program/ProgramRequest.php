<?php

namespace App\Http\Requests\Program;

use Illuminate\Foundation\Http\FormRequest;

class ProgramRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'p_heading' => 'required',
            'p__brief' => 'required',
            'p__image' => 'mimes:jpeg,png,jpg|max:5000',
            'get__image' => 'mimes:jpeg,png,jpg|max:5000',
        ];
    }

    public function messages()
    {
        return [
            'p_heading.required' => 'Please fill in the box',
            'p__brief.required' => 'Please fill in the box',

            'p__image.mimes' => 'File type should be jpeg, jpg or png',
            'p__image.max' => 'File size should be 5MB per image',

            'get__image.mimes' => 'File type should be jpeg, jpg or png',
            'get__image.max' => 'File size should be 5MB per image',
        ];
        
    }
}
