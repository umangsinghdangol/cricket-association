<?php

namespace App\Http\Requests\About;

use Illuminate\Foundation\Http\FormRequest;

class AboutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'aboutUsContent'=>'required',
            'visionSectionContent'=>'required',
            "v_image" => 'mimes:jpeg,png,jpg|max:5000',
            'missionSectionContent'=>'required',
            "m_image" => 'mimes:jpeg,png,jpg|max:5000',
        ];
    }

    public function messages()
    {
        return [
            'aboutUsContent.required' => 'Please fill in the box',

            'visionSectionContent.required' => 'Please fill in the box',

            'v_image.mimes' => 'File type should be jpeg, jpg or png',
            'v_image.max' => 'File size should be 5MB per image',

            'missionSectionContent.required' => 'Please fill in the box',

            'm_image.mimes' => 'File type should be jpeg, jpg or png',
            'm_image.max' => 'File size should be 5MB per image',

        ];
        
    }
}
