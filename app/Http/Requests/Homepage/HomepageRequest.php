<?php

namespace App\Http\Requests\Homepage;

use Illuminate\Foundation\Http\FormRequest;

class HomepageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'get__heading' => 'required',
            // 'get__subheading' => 'required',
            // "get__image" => ["array","min:0","max:10"],
            // 'get__image.*'=>'mimes:jpeg,png,jpg|max:5000',
        
        ];
    }

    public function messages()
    {
        return [
            // 'get__heading.required' => 'Please fill in the box',

            // 'get__subheading.required' => 'Please fill in the box',

            // 'get__image.*.mimes' => 'File type should be jpeg, jpg or png',
            // 'get__image.*.max' => 'File size should be 5MB per image',
        ];
        
    }
}
