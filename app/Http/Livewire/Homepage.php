<?php

namespace App\Http\Livewire;

use App\Repository\Additional\Converter;
use Livewire\Component;
use Livewire\WithFileUploads;
use App\Repository\Homepage\HomepageRepository;
use Illuminate\Support\Facades\DB;

class Homepage extends Component
{
    use WithFileUploads;
    protected $homepageRepository;
    protected $converter;
    public $inputs = [];
    public $i = 0;
    public $updateMode = false;
    public $fuck=[];
    private $average;
    public $all_homepage;

    public function mount(HomepageRepository $homepageRepository,Converter $converter)
    {
        $this->homepageRepository = $homepageRepository;
        $this->converter=$converter;
        $this->all_homepage = $this->homepageRepository->allHome();
    }
    public function render(){
        return view('livewire.homepage');
    }

    public function add($i){
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs ,$i);
    }

    public function remove($i){
        $this->inputfieldsclearspecific($this->inputs[$i]);
        unset($this->inputs[$i]);
    }

    private function inputfieldsclearspecific($i){
        $this->fuck[$i]['get__heading'] = null;
        $this->fuck[$i]['get__subheading'] = null;
        $this->fuck[$i]['get__image'] = '0';

    }

     private function resetInputFields()
     {
        foreach($this->fuck as &$items){
            $items['get__heading'] = null;
            $items['get__subheading'] = null;
            $items['get__image'] = null;
        }
     }

     protected $rules = [
        'fuck.*.get__heading' => 'required',
        'fuck.*.get__subheading' => 'required',
    ];

    public function store(HomepageRepository $homepageRepository){
        $this->validate();
        $this->homepageRepository = $homepageRepository;
        foreach($this->fuck as $items){
            $this->homepageRepository->createHomepage($items);
        }

        $this->all_homepage = $this->homepageRepository->allHome();
        $this->inputs = [];
        $this->resetInputFields();
    }

}
